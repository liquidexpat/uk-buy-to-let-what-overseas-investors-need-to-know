Demand for UK expat buy to let mortgages to purchase UK property, has soared over the last 12 months. Despite stricter stress-testing regulations brought in by the Prudential Regulation Authority (PRA) in January 2017, expats and overseas investors remain undeterred by this. 

Thanks to a weak pound, post Brexit the UK property market is becoming an increasingly attractive investment opportunity for expats and overseas investors. This trend is likely to continue in line with the recent forecast for the growth of global expatriates.

For expats, buy to let is often a great way to keep a foothold in the UK and the yields on buy to let properties in the UK are far ahead of those offered by other countries. This increased interest in UK buy to let mortgages is in spite of increasing initiatives by the UK government to dampen buy to let purchases, such as the second home stamp duty and increasing stress testing for buy to let mortgages.

Research the buy-to-let market

Investing in buy-to-let involves committing money to a property and typically taking out a mortgage, normally putting down a deposit of circa 25%. When house prices rise, this means it is possible to make big leveraged gains above your mortgage debt.

Property investing has paid off handsomely for many people, both in terms of income and capital gains but it is essential that you go into it with your eyes wide open, acknowledging the potential advantages and disadvantages. Speak to a specialist mortgage broker for some impartial advice and knowledge to help you make an informed decision and understand the buy to let mortgage products available to you. The more knowledge you have and the more research you do, the better the chance of your investment paying off.

Choose wisely where to invest

Choosing a promising area to invest in does not necessarily mean the most expensive or cheapest, instead focus on choosing somewhere that people want to live. Consider if new employers are coming to the area that you’re looking to invest in, will this impact demand for rental housing in the area? If you’re looking at a commuter belt location, are transport links readily available? 

If you’re not sure where to invest in the UK, talk to an expert. It helps to keep your process moving if you have an understanding of what your finance options are and what you can afford to invest in. You need to match the kind of property you can afford and want to buy with the locations that people would want to live. 

Know your expat buy to let regulation

Mortgages have been affected by rules introduced in 2014, which made things tricky for all potential borrowers whose income and expenditure had to be evidenced after this time. However, for expats things are tougher still. 

Following European rules that came into place in 2016, individuals paid in a foreign currency must now come under closer scrutiny when their applications are assessed by mortgage underwriters. Exchange rate fluctuations are taken into consideration- not just the current value of the currency and many lenders assess a potential borrower's global financial position in their decision-making process. This is a form of stress testing that is applied to buy to let applications to assess overall affordability and ensure that they feel comfortable that the new mortgage will not put them under undue financial stress now, or in the future. 

Review your options

Buy to let lending is where the market is most buoyant for expats. New entrants are offering mortgages based on very flexible criteria with competitively priced mortgage deals. As a rule, you will need a minimum 25 per cent deposit for UK buy to let, with the rental income effectively defining how much you can borrow. Most lenders require the property to wash its face, which means it typically needs the rental income to exceed the mortgage payments by a ratio of 145 per cent if the mortgage rate was 5.5 per cent. 

Using a mortgage broker who specialises in expat mortgages will open a plethora of potential mortgage options, whether you are looking for a buy to let investment, or for a home for your family to dwell in whilst you are based overseas. Not only will they have access to exclusive products, but they will also help to manage the application process from start to finish.
https://www.liquidexpatmortgages.com/